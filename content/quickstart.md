---
title: Quick start
---

## Quick start with sourcehut pages

The steps for publishing your site are:

1. [Generate a personal access token][pat]
2. Create a tarball with your website's contents.
3. Upload your tarball to pages.sr.ht

Once you're live, we'll look at ways to automate the process.

### Your personal access token

You can [generate a personal access token][pat] on meta.sr.ht. Write it down for
later reference.

[pat]: https://meta.sr.ht/oauth2/personal-token

### Building your tarball

Here's a simple example. Let's say you write this file to `index.html`:

```html
<!doctype html>
<html lang="en">
<meta charset="utf-8" />
<title>My sourcehut page</title>
<h1>My sourcehut page</h1>
<p>Welcome to my cool sourcehut page!
```

You can create a tarball for it like so:

```
$ tar -cvz index.html > site.tar.gz
```

Add any number of files other than index.html to expand this.

**Using a static site generator**

Here's another example for a website using [Hugo](https://gohugo.io):

```
$ hugo
$ tar -C public -cvz . > site.tar.gz
```

You can use any tool, just so long as it outputs static content.

### Uploading your tarball

Once you have a tarball, upload it to pages.sr.ht. Install and configure
[hut](https://sr.ht/~xenrox/hut/), then run:

```
$ hut pages publish -d username.srht.site site.tar.gz
```

Substitute "username" with your sr.ht username.

<div class="event">
  Want to use your own domain name? <a href="/custom-domains">Review the extra
  steps here</a>.
</div>

Once you run this command, your website should now be live at
`https://username.srht.site`! The first load will take an extra second or two
while we obtain a TLS certificate for you.

<div class="alert alert-info call-to-action" style="align-items: flex-start">
  <p>
    Need some help building your site?
      <a href="https://gohugo.io" rel="noopener">Hugo</a>
    is a good choice for getting up and running quickly with a nice theme for
    your site. We recommend it for users new to static site generation. Feel
    free to set up a site you like, then come back when you're ready to publish
    it.
  </p>
  <a href="https://gohugo.io/getting-started/quick-start/" class="btn btn-primary" rel="noopener">
    Hugo Quick Start
    <span class="icon">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path d="M0 384.662V127.338c0-17.818 21.543-26.741 34.142-14.142l128.662 128.662c7.81 7.81 7.81 20.474 0 28.284L34.142 398.804C21.543 411.404 0 402.48 0 384.662z"/></svg>
    </span>
  </a>
</div>

## Next: Automating deployments

Next: <a href="/automating-deployments">Automating deployments <span class="icon">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path d="M0 384.662V127.338c0-17.818 21.543-26.741 34.142-14.142l128.662 128.662c7.81 7.81 7.81 20.474 0 28.284L34.142 398.804C21.543 411.404 0 402.48 0 384.662z"/></svg>
</span></a>
