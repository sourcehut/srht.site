---
title: GraphQL API
---

## GraphQL API

pages.sr.ht offers a GraphQL API that you can use to manage sites &mdash;
publishing, listing, and unpublishing them. It follows the standard [sourcehut
GraphQL conventions](https://man.sr.ht/graphql.md), and you can find its schema
[here](https://git.sr.ht/~sircmpwn/pages.sr.ht/tree/master/item/graph/schema.graphqls).

Note that the pages.sr.ht GraphQL API does not support legacy OAuth - you have
to use OAuth 2.0 tokens.
