---
title: Automating deployments
---

## Automating deployments

It's not a good idea to repeat the [quick start process](/quickstart) every
time you want to update your site. So let's automate it!

### Create your repository

First, create a repository on [git.sr.ht](https://git.sr.ht/create) or
[hg.sr.ht](https://hg.sr.ht/create) named after your website (e.g.
"your-username.srht.site"), and push your website's source code to it.

<div class="alert alert-info">
  Unfamiliar with git or hg? <a href="https://man.sr.ht/tutorials/set-up-account-and-git.md">Check out the tutorial</a>.
</div>

### Writing the build manifest

We'll be using [builds.sr.ht](https://man.sr.ht/builds.sr.ht) to automate
building and deploying our site. Create a file named ".build.yml" and fill it in
with this template:

```
image: alpine/edge
oauth: pages.sr.ht/PAGES:RW
packages:
- hut
environment:
  site: your-username.srht.site
tasks:
- package: |
    cd $site
    tar -cvz . > ../site.tar.gz
- upload: |
    hut pages publish -d $site site.tar.gz
```

<div class="event">
  <a href="https://sr.ht/~xenrox/hut/">hut</a> is a CLI tool to interact with
  the sr.ht APIs. It uses an OAuth 2.0 token generated on-the-fly with the
  permissions requested by the "oauth" directive - in this case, only enough to
  deploy your site.
</div>

Update the `site` variable to your desired website (e.g.
"your-username.srht.site"). You can also add additional steps under `tasks`
and `package` to call a static site generator and bundle your website
differently. Once you're done, commit this file to your repository and push. A
URL will be printed to your console which you can click on to follow the
progress on deploying your site. Once it's done, that's it - your site will be
re-published every time you push.

<div class="alert alert-info">
  Not sure how to build your static site with a build manifest? We have some
  examples for common static site generators available in the <a
  href="https://git.sr.ht/~sircmpwn/pages.sr.ht-examples">example pages
  repository</a>.
</div>

For more information about using builds.sr.ht, consult
[the builds.sr.ht documentation](https://man.sr.ht/builds.sr.ht).
