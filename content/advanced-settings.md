---
title: Advanced settings
---

## SiteConfig file

With a custom SiteConfig file (in `json` format) it is possible to configure your own 404 error page and Cache-Control headers.
The Cache-Control headers will be applied to every file that matches `glob`.
`notFound` is the path to your custom 404 page.

```json
{
    "notFound": "404.html",
    "fileConfigs": [
        { "glob": "*.png", "options": { "cacheControl": "max-age=15552000" } },
        { "glob": "*.css", "options": { "cacheControl": "max-age=7200" } }
    ]
}
```

### Deploying with hut

You can use [hut] to easily deploy your website with your `siteconfig.json`:

```
hut pages publish -d example.com --site-config siteconfig.json example.tar.gz
```

[hut]: https://sr.ht/~xenrox/hut
