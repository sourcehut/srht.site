---
title: sourcehut pages
---

sourcehut pages is a tool which allows you to publish static websites. Every
[sourcehut](https://sourcehut.org) user receives the `username.srht.site`
domain, and you can bring your own domain as well.

- Any content generator, including Jekyll, Hugo, Doxygen, or your cool new one
- Automatic, zero-config TLS
- Use our domain or BYOD

<div class="call-to-action" style="margin-bottom: 2rem">
  <p>Ready to get started? You can get published in 3 minutes.</p>
  <a href="/quickstart" class="btn btn-primary">
    Quick start
    <span class="icon">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path d="M0 384.662V127.338c0-17.818 21.543-26.741 34.142-14.142l128.662 128.662c7.81 7.81 7.81 20.474 0 28.284L34.142 398.804C21.543 411.404 0 402.48 0 384.662z"/></svg>
    </span>
  </a>
</div>

### Resources for website builders

- [Quick start](/quickstart)
- [Custom domains](/custom-domains)
- [Automating deployments](/automating-deployments)
- [Advanced settings](/advanced-settings)
- [Limitations](/limitations)
- [REST API](https://man.sr.ht/pages.sr.ht/api.md)
- [GraphQL API](/graphql)
- [Gemini support](gemini://srht.site)
