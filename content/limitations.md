---
title: Limitations
---

## Limitations

You can publish almost any kind of content (limited by the [terms of service][0],
of course), but there are some technical limitations in place via a
content-security policy which is applied to all sr.ht pages sites. Our full CSP
header is:

[0]: https://man.sr.ht/terms.md

```
Content-Security-Policy: 
  default-src 'self' data: blob:;
  script-src 'self' 'unsafe-eval' 'unsafe-inline';
  style-src 'self' 'unsafe-inline';
  worker-src 'self';
  frame-src https:;
  img-src https:;
  media-src https:;
  object-src 'none';
  sandbox allow-forms allow-pointer-lock allow-presentation allow-same-origin allow-scripts allow-popups;
```

In short, the effect of this policy is:

- All scripts and styles must be loaded from your own site, not a CDN
- Tracking scripts are not permitted
- External media (images, audio, video) must be loaded with HTTPS

The published tarball is limited to 1 GiB in size, after decompression. Any
entries other than regular files are ignored (such as symlinks).
