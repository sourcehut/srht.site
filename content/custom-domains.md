---
title: Custom domains 
---

## Custom domains with sourcehut pages

In order to use a custom domain, you must first configure your website's DNS
records accordingly. You can do this at your domain registrar's dashboard.

### Top-level domains

To add a domain like `example.org`, you need to add an A record which points to
`46.23.81.157` and an AAAA record which points to `2a03:6000:1813:1337::157`.

```
@ IN A    46.23.81.157
@ IN AAAA 2a03:6000:1813:1337::157
```

<div class="alert alert-danger">
  We reserve the right to change this IP address. If your site will be affected, 
  we'll send you an email at least 30 days in advance.
  Make sure your email address
  <a href="https://meta.sr.ht/profile">is up to date</a>!
</div>

That's it! Just substitute your domain name in place of `username.srht.site`
when you publish. <a href="/quickstart">Back to the quick start <span class="icon">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path d="M0 384.662V127.338c0-17.818 21.543-26.741 34.142-14.142l128.662 128.662c7.81 7.81 7.81 20.474 0 28.284L34.142 398.804C21.543 411.404 0 402.48 0 384.662z"/></svg>
</span></a>

### Second-level domains

Simply add a CNAME record which points to `pages.sr.ht.` (the period at the end
is important!). For example, to add foobar.example.org (in bind format):

```
foobar IN CNAME pages.sr.ht.
```

**NOTE**: Some (bad) domain registrars will complain about the trailing period.
If this happens, just remove it, and it'll probably work. If it doesn't work,
please send an angry email to your crappy domain registrar.

That's it! Just substitute your domain name in place of `username.srht.site`
when you publish. <a href="/quickstart">Back to the quick start <span class="icon">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512"><path d="M0 384.662V127.338c0-17.818 21.543-26.741 34.142-14.142l128.662 128.662c7.81 7.81 7.81 20.474 0 28.284L34.142 398.804C21.543 411.404 0 402.48 0 384.662z"/></svg>
</span></a>
